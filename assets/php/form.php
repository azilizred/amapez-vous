<!DOCTYPE HTML>
<html>
	<head>
		<title>AMAPEZ-VOUS ! | Message envoyé</title>
		<link rel="shortcut icon" href="images/iconetrans.png" type="image/x-icon" />
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>
    <?php
// S'il y des données de postées
if ($_SERVER['REQUEST_METHOD']=='POST') {

  // (1) Code PHP pour traiter l'envoi de l'email

  // Récupération des variables et sécurisation des données
	$prenom     = htmlentities($_POST['prenom']);
  $nom     = htmlentities($_POST['nom']); // htmlentities() convertit des caractères "spéciaux" en équivalent HTML
  $email   = htmlentities($_POST['email']);
  $monamap = htmlentities($_POST['monamap']);
  $adresse = htmlentities($_POST['adresse']);
  $adherents = htmlentities($_POST['adherents']);
  $web = htmlentities($_POST['web']);
  $message = htmlentities($_POST['message']);

  // Variables concernant l'email
  $destinataire = 'lisamambre96@gmail.com'; // Adresse email du webmaster (à personnaliser)
  $sujet = 'Référencement AMAP sur AMAPEZ-vous !'; // Titre de l'email
  $contenu = '<html><head><title>Titre du message</title></head><body>';
  $contenu .= '<p>Bonjour, vous avez reçu un message à partir de votre site web AMAPEZ-vous !</p>';
	$contenu .= '<p><strong>Prénom</strong>: '.$prenom.'</p>';
  $contenu .= '<p><strong>Nom</strong>: '.$nom.'</p>';
  $contenu .= '<p><strong>Email</strong>: '.$email.'</p>';
  $contenu .= '<p><strong>Nom AMAP</strong>: '.$monamap.'</p>';
  $contenu .= '<p><strong>Adresse</strong>: '.$adresse.'</p>';
  $contenu .= '<p><strong>Nombre adhérents</strong>: '.$adherents.'</p>';
  $contenu .= '<p><strong>Site web</strong>: '.$web.'</p>';
  $contenu .= '<p><strong>Message</strong>: '.$message.'</p>';
  $contenu .= '</body></html>'; // Contenu du message de l'email (en XHTML)

  // Pour envoyer un email HTML, l'en-tête Content-type doit être défini
  $headers = 'MIME-Version: 1.0'."\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1'."\r\n";

  // Envoyer l'email
    mail($destinataire, $sujet, $contenu, $headers); // Fonction principale qui envoi l'email

		echo '<h3>Message envoyé !</h3>'; // Afficher un message pour indiquer que le message a été envoyé
  // (2) Fin du code pour traiter l'envoi de l'email

?>

<section id="three" class="wrapper style4 special fade">
  <a href="formulaire.html" class="button special">Retour</a>
</section>
</body>
</html>
